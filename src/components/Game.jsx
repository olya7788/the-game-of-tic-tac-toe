
import React, {useState} from 'react';
import Board from './board';
import './Game.css'
import {calculaterWinner} from '../helper'


const Game = () => {
    const [board, setBoard] = useState(Array(9).fill('null')) 
    const [xIsNext, setIsNext] = useState(true)
    const winner = calculaterWinner(board)
const handleClick = (index) =>{
    const boardCopy = [...board]

// определить был ли клик по ячейки или игра закончена
if (winner || boardCopy[index]) 
//определить чей ход x ? 0
boardCopy[index] = xIsNext ? 'x' :'0'

// обновить наш стейт
setBoard(boardCopy)
setIsNext(! xIsNext)
}
const startNewGame = () => {
    return (
        <button className='start_btn' onClick={() => setBoard(Array(9).fill(null))} >Очистить поле</button>
    )
}

    return (
        <div className='wrapper'>
            {startNewGame() }
            <Board squares={board} click={handleClick} />
            <p className='game_info'>
                { winner ? 'Победитель '  + winner : 'Сейчас ходит ' + (xIsNext ? 'x': '0') }
            </p>
        </div>
    );
}

export default Game;
